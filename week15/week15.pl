use strict;
use warnings;

my %grades = ("Ahmet"=> 80, "Zeynep"=>50, "Kemal"=>100);

print("sort by key alphabetical order");
foreach my $student (sort keys %grades){
	print "$student $grades{$student}\n";
	
}
print("sort by key numerical order");
foreach my $student (sort {$grades{$a} <=> $grades{$b}} keys %grades){
	print "$student $grades{$student}\n";
	
}


###two dimensional array


my @a = ([1,2,3], [4,5,6], [7,8,9]);

print "$a[2][1] $a[1][2]\n";

###array of hash example

my @AoH = 
		({husband=>"barney" , wife=> "betty",son=>"bam bam" },
		{husband=>"george", wife=>"jane" ,son=>"elroy" },
		{husband=> "homer", wife=>"marge" ,son=>"bart" });
			
